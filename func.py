from math import log

def g_func(x,a):
    return (7 * (4 * a ** 2 + 15 * a * x - 4 * x ** 2)) / (-45 * a)

def f_func(x,a):
    return 2 ** (40 * a ** 2 - 107 * a * x + 63 * x ** 2)

def y_func(x,a):
    return log(-a ** 2 - 2 * a * x + 3 * x ** 2 + 1)