import matplotlib.pyplot as plt
import math


while True:
    my_function = input('The function you want to count: ')
    if my_function in ['g', 'f', 'y']:
        pass
    else:
        print('Incorrect function name')
        continue

    x_list = []
    y_list = []

    x = a = x_max = x_step = 0

    try:
        x = float(input('Enter the value of the variable x for the function: '))
        a = float(input('Enter the value of variable a for the function: '))
        x_max = float(input('Enter the maximum x value: '))
        x_step = float(input('Enter the number of steps: '))
    except ValueError:
        print('You entered an invalid value ("ValueError")')


    if my_function == 'g':
        while x < x_max:
            try:
                g = (7 * (4 * a ** 2 + 15 * a * x - 4 * x ** 2))/(-45 * a ** 2 + 26 * a * x + 7 * x ** 2)
                x_list.append(x), y_list.append(g)
                x += x_step
                print(f'x = {x} \t y = {g}\n')
            except ZeroDivisionError:
                x_list.append(x), y_list.append(None)
                print(f'x = {x} \t y = {None}')

    if my_function == 'f':
        while x < x_max:
            try:
                f = 2 ** (40 * a ** 2 - 107 * a * x + 63 * x ** 2)
                x_list.append(x), y_list.append(f)
                x += x_step
                print()
                print(f'x = {x}, y = {f}\n')
            except OverflowError or ValueError:
                x_list.append(x), y_list.append(None)
                print(f'x = {x} \t y = {None}')

    if my_function == 'y':
        if x >= -1:
            while x < x_max:
                try:
                    y = math.log(-a ** 2 - 2 * a * x + 3 * x ** 2 + 1)
                    x_list.append(x), y_list.append(y)
                    x += x_step
                    print()
                    print(f'x = {x} \t y = {y}\n')
                except ValueError:
                    x_list.append(x), y_list.append(None)
                    print(f'x = {x} \t y = {None}')


    plt.plot(x_list, y_list, 'k')
    plt.axis('tight')
    plt.show()
    y_list = list(filter(None, y_list))

    print('Min element x_list:', min(x_list))
    print('Max element x_list:', max(x_list))
    print('Min element y_list:', min(y_list))
    print('Max element y_list:', max(y_list))

    new = input('Do you want to log out? yes / no ')
    if new == 'yes':
        break
    elif new == 'no':
        pass
    else:
        exit('You entered an invalid value')
