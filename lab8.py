import random
import lib_count
import matplotlib.pyplot as plt
import matplotlib.patches as pch
try:
    number = int(input('Amount of points : '))
    rad = float(input('Rad : '))
except ValueError:
    print('No value found')

coordinates = [(random.uniform(-50, 50), random.uniform(-50, 50)) for i in range(number)]
center = (random.uniform(-50, 50), random.uniform(-50, 50))

print(f'Точек : {lib_count.search(coordinates, center, rad)}')

ax = plt.subplot()
ax.scatter(center[0], center[1], c='red', s=11, zorder=100, marker='X')
ax.scatter([x for x, y in coordinates if lib_count.in_area((x, y), center, rad) is True],
           [y for x, y in coordinates if lib_count.in_area((x, y), center, rad) is True],
           s=9,
           c='deeppink',
           zorder=50,
           marker='p')
ax.scatter([x for x, y in coordinates if lib_count.in_area((x, y), center, rad) is False],
           [y for x, y in coordinates if lib_count.in_area((x, y), center, rad) is False],
           s=3,
           c='darkorange',
           zorder=0)

ax.add_patch(pch.Circle(center, radius=rad, fill=False))
ax.axis('square')

plt.show()
