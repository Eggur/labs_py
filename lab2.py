import math
#Выбор выполнения одной из функции
choice = int(input('Which function do you want to choose? G, F, Y?: '))
#Ввод значений x и a
x = float(input("Enter x: "))
a = float(input("Enter a: "))
#Решение функции G
if choice == 1:
    try:
        G = (7 * (4 * a ** 2 + 15 * a * x - 4 * x ** 2)) / (-45 * a ** 2 + 26 * a * x + 7 * x ** 2)
        print("G =", G)
    except ValueError:
        print("You gave wrong values for chosen function")
#Вариант с F
elif choice == 2:
    try:
        F = 2 ** (40 * a ** 2 - 107 * a * x + 63 * x ** 2)
        print("F =", F)
    except ValueError:
        print("You gave wrong values for chosen function")
#Вариант с Y
elif choice == 3:
    try:
        Y = math.log(-a ** 2 - 2 * a * x + 3 * x ** 2 + 1)
        print("Y =", Y)
    except ValueError:
        print("You gave wrong values for chosen function")
#Вариант, когда выбран неправильный номер функции
if choice in [4, 5, 6, 7, 8, 9]:
    print("This function does not exist")
