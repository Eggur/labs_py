import matplotlib.pyplot as plt
import math

while True:
    my_function = input('The function you want to count: ')
    if my_function in ['g', 'f', 'y']:
        pass
    else:
        exit('Incorrect function name')

    x_list = []
    y_list = []

    try:
        x = float(input('Enter the value of the variable x for the function: '))
        a = float(input('Enter the value of variable a for the function: '))
        x_max = float(input('Enter the maximum x value: '))
        x_step = float(input('Enter the number of steps: '))
    except ValueError:
        print('You entered an invalid value ("ValueError")')
        break

    if my_function == 'g':
        while x < x_max:
            try:
                g = (7 * (4 * a ** 2 + 15 * a * x - 4 * x ** 2)) / (-45 * a ** 2 + 26 * a * x + 7 * x ** 2)
                x_list.append(x), y_list.append(g)
                x += x_step
            except ZeroDivisionError:
                x_list.append(x), y_list.append(None)
                print('You divided by zero')
                break

    if my_function == 'f':
        while x < x_max:
            try:
                f = 2 ** (40 * a ** 2 - 107 * a * x + 63 * x ** 2)
                x_list.append(x), y_list.append(f)
                x += x_step
            except ValueError:
                x_list.append(x), y_list.append(None)
                break

    if my_function == 'y':
        if x >= -1:
            while x < x_max:
                try:
                    y = math.log(-a ** 2 - 2 * a * x + 3 * x ** 2 + 1)
                    x_list.append(x), y_list.append(y)
                    x += x_step
                except ValueError:
                    x_list.append(x), y_list.append(None)
                    break

    print(x_list)
    print(y_list)
    plt.plot(x_list, y_list, 'k')
    plt.axis('tight')
    plt.show()

    new = input('Do you want to log out? yes / no ')
    if new == 'yes':
        break
    elif new == 'no':
        pass
    else:
        exit('You entered an invalid value')
